export const main_server_url = "https://appdevelopment.codemecorrect.com/api";
export const server_upload_url = "https://appdevelopment.codemecorrect.com";
// export const server_upload_url = "https://dev.cwdgservices.com";
// export const main_server_url = "https://dev.cwdgservices.com/api";
export const site_url = "https://ionicframework.com/";

// variables for localstorage
export const APP_PREFIX = "wireframe_";
export const SELECTED_LANG = APP_PREFIX + "sel_lang";
export const USERFORM_SIGNUP = APP_PREFIX + "userform_signup";
export const CURRENTAPPUSERTOKEN = APP_PREFIX + "current_appuser_token";
export const CURRENTAPPUSER = APP_PREFIX + "current_appuser";
export const TRAVEL_COUNTRY = APP_PREFIX + "travel_country";
export const SELECTED_HOST_MARKER = APP_PREFIX + "selected_host_marker";

export const LANGUAGES = [
    { value: 'en', text: 'English' },
    { value: 'he', text: 'Hebrew' }
];

export const COUNTRIES = [
    "Israel",
    "Japan",
    "United state",
    "China",
    "Romania",
    "Russia",
];

export class TravelCountryClass {
    id: string;
    text: string;
    small_name: string;
    lat: string;
    lng: string;
    zoom: string;
    constructor(travelcountry) {
        this.id = travelcountry.id || '';
        this.text = travelcountry.text || '';
        this.small_name = travelcountry.small_name || '';
        this.lat = travelcountry.lat || '';
        this.lng = travelcountry.lng || '';
        this.zoom = travelcountry.zoom || '';
    }
}
export class AppUserClass {
    id: string;
    email: string;
    password: string;
    active: string;
    first_name: string;
    last_name: string;
    phone_number: string;
    whatsapp_number: string;
    address: string;
    address2: string;
    city: string;
    state: string;
    postal_code: string;
    country: string;
    photo: any;

    constructor(appUser) {
        this.id = appUser.id || '';
        this.email = appUser.email || '';
        this.password = appUser.password || '';
        this.active = appUser.active || '';
        this.first_name = appUser.first_name || '';
        this.last_name = appUser.last_name || '';
        this.phone_number = appUser.phone_number || '';
        this.whatsapp_number = appUser.whatsapp_number || '';
        this.address = appUser.address || '';
        this.address2 = appUser.address2 || '';
        this.city = appUser.city || '';
        this.state = appUser.state || '';
        this.postal_code = appUser.postal_code || '';
        this.country = appUser.country || '';
        this.photo = appUser.photo || '';

        // this.id = appUser.id || '';
        // this.email = appUser.email || 'aaaa@outlook.com';
        // this.password = appUser.password || 'Aaaa1111';
        // this.active = appUser.active || 'Aaaa1111';
        // this.first_name = appUser.first_name || 'aaaa';
        // this.last_name = appUser.last_name || 'bbbb';
        // this.phone_number = appUser.phone_number || '1231231';
        // this.whatsapp_number = appUser.whatsapp_number || 'awse1123';
        // this.address = appUser.address || 'asdfasd';
        // this.address2 = appUser.address2 || '';
        // this.city = appUser.city || 'asdfsd';
        // this.state = appUser.state || '';
        // this.postal_code = appUser.postal_code || '1232';
        // this.country = appUser.country || '';
        // let fileurl = appUser.photo;
        // this.photo = fileurl || '';
    }
}

export function getPhotoUrl(url) {
    return url ? server_upload_url + url.replace('..', '') : '';
}
export class HostMarkerClass {
    // listings
    id: string;
    userid: string;
    lat: string;
    lon: string;
    address: string;
    address2: string;
    suburb: string;
    city: string;
    state: string;
    postal_code: string;
    region: string;
    suspended: string;
    imagefolder: string;
    image1: string;
    image2: string;
    image3: string;
    image4: string;
    image5: string;

    // user
    email: string;
    role: string;
    name: string;
    bio: string;
    approved: string;
    approved_date: string;
    active: string;
    last_login: string;
    register_date: string;
    ip_address: string;
    agree: string;
    step: string;
    vuid: string;
    primary_contact_option: string;
    primary_contact: string;
    secondary_contact_option: string;
    secondary_contact: string;
    other_contact_option: string;
    other_contact: string;
    profile_image: string;


    constructor(hostMarker) {
        this.id = hostMarker.id || '';
        this.userid = hostMarker.userid || '';
        this.lat = hostMarker.lat || '';
        this.lon = hostMarker.lon || '';
        this.address = hostMarker.address || '';
        this.address2 = hostMarker.address2 || '';
        this.suburb = hostMarker.suburb || '';
        this.city = hostMarker.city || '';
        this.state = hostMarker.state || '';
        this.postal_code = hostMarker.postal_code || '';
        this.region = hostMarker.region || '';
        this.suspended = hostMarker.suspended || '';
        this.imagefolder = hostMarker.imagefolder || '';
        this.image1 = getPhotoUrl(hostMarker.image1) || '';
        this.image2 = getPhotoUrl(hostMarker.image2) || '';
        this.image3 = getPhotoUrl(hostMarker.image3) || '';
        this.image4 = getPhotoUrl(hostMarker.image4) || '';
        this.image5 = getPhotoUrl(hostMarker.image5) || '';

        this.id = hostMarker.id || '';
        this.email = hostMarker.email || '';
        this.role = hostMarker.role || '';
        this.region = hostMarker.region || '';
        this.name = hostMarker.name || '';
        this.bio = hostMarker.bio || '';
        this.approved = hostMarker.approved || '';
        this.approved_date = hostMarker.approved_date || '';
        this.active = hostMarker.active || '';
        this.last_login = hostMarker.last_login || '';
        this.register_date = hostMarker.register_date || '';
        this.ip_address = hostMarker.ip_address || '';
        this.agree = hostMarker.agree || '';
        this.step = hostMarker.step || '';
        this.vuid = hostMarker.vuid || '';
        this.primary_contact_option = hostMarker.primary_contact_option || '';
        this.primary_contact = hostMarker.primary_contact || '';
        this.secondary_contact_option = hostMarker.secondary_contact_option || '';
        this.secondary_contact = hostMarker.secondary_contact || '';
        this.other_contact_option = hostMarker.other_contact_option || '';
        this.other_contact = hostMarker.other_contact || '';
        this.profile_image = hostMarker.profile_image || '';
    }
}
// match users table
export class Listingoptions {
    id: string
    userid: string
    number_of_beds: string
    bed_configure: string
    internet: string
    private_restroom: string
    kitchen: string
    shared_restroom: string
    transportation: string
    grocery: string
    off_street_parking: string
    city_center: string
    airport: string
    camper_parking: string
    tenting: string
    laundry: string
    no_drugs: string
    no_alcohol: string
    no_smoking: string
    no_unmarried: string
    no_after_9: string
    locked_after_10: string

    constructor(user) {
        this.id = user.id || '';
        this.userid = user.userid || '';
        this.number_of_beds = user.number_of_beds || '';
        this.bed_configure = user.bed_configure || '';
        this.internet = user.internet || '';
        this.private_restroom = user.private_restroom || '';
        this.kitchen = user.kitchen || '';
        this.shared_restroom = user.shared_restroom || '';
        this.transportation = user.transportation || '';
        this.grocery = user.grocery || '';
        this.off_street_parking = user.off_street_parking || '';
        this.city_center = user.city_center || '';
        this.airport = user.airport || '';
        this.camper_parking = user.camper_parking || '';
        this.tenting = user.tenting || '';
        this.laundry = user.laundry || '';
        this.no_drugs = user.no_drugs || '';
        this.no_alcohol = user.no_alcohol || '';
        this.no_smoking = user.no_smoking || '';
        this.no_unmarried = user.no_unmarried || '';
        this.no_after_9 = user.no_after_9 || '';
        this.locked_after_10 = user.locked_after_10 || '';
    }
}