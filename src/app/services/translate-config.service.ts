import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class TranslateConfigService {

  constructor(private translate: TranslateService) { }

  getDefaultLanguage() {
    let lang = this.translate.getBrowserLang();
    console.log(lang);
    this.translate.setDefaultLang(lang);
    return (lang);
  }

  setLanguage(setLang) {
    console.log("set lang:", setLang);

    this.translate.use(setLang);
  }
}
