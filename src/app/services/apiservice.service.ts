import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { main_server_url, AppUserClass } from './common';
import { LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { CommonService } from './common.service';
@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  httpHeaders: HttpHeaders;
  constructor(private http: HttpClient, private loadingController: LoadingController, private commonService: CommonService) {
    this.httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

    let options = { headers: this.httpHeaders };
    let body = { email: 'email' };
    // this.http.post<any>(main_server_url + '/upload', body, options).subscribe(res => {
    //   console.log("image upload :", res);
    // }, err => {
    //   console.error("image upload error:", err);

    // });
  }

  uploadImageData(ba64: string, id: any) {
    let httpHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    let options = { headers: httpHeaders };
    let newEncodedB64 = ba64.substring('data:image/jpeg;base64,'.length, ba64.length)
    let body = {
      'b64': newEncodedB64,
      id: id
    }
    return this.http.post(main_server_url + '/upload', body, options);
  }

  login(uName, uPwd) {
    let httpHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    let options = { headers: httpHeaders };
    let body = {
      'username': uName,
      'password': uPwd
    }
    return this.http.post(main_server_url + '/login', body, options);
  }
  forgotPwd(email) {
    let options = { headers: this.httpHeaders };
    let body = { email: email };
    return this.http.post<any>(main_server_url + '/forgotPassword', body, options);
  }
  checkRCode(userid, code) {
    let options = { headers: this.httpHeaders };
    let body = {
      userid: userid,
      code: code
    };
    return this.http.post<any>(main_server_url + '/checkRecoveryCode', body, options);
  }
  checkFKey(email, pwd, code) {
    let options = { headers: this.httpHeaders };
    let body = {
      email: email,
      password: pwd,
      code: code
    };
    return this.http.post<any>(main_server_url + '/checkForgotKey', body, options);
  }
  isExistingEmail(email) {
    let options = { headers: this.httpHeaders };
    let body = { email: email };
    return this.http.post<any>(main_server_url + '/isExsitingEmail', body, options);
  }
  createNewPwd(userId, pwd) {
    let options = { headers: this.httpHeaders };
    let body = { password: pwd };
    return this.http.post<any>(main_server_url + '/generatePassword', body, options);
  }
  signup(newUser: AppUserClass) {
    let httpHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    let options = {
      headers: httpHeaders
    };
    let body = newUser;
    return this.http.post<any>(main_server_url + '/createAppUser', body, options);
  }
  // update profile
  updateProfile(user: any) {
    let options = { headers: this.httpHeaders };
    let body: any = user;
    body.token = this.commonService.getCurrentToken();
    return this.http.post<any>(main_server_url + '/updateAppUser', body, options);
  }
  // update profile photo
  updateProfilePhoto(photo: any) {
    let options = { headers: this.httpHeaders };
    let body = { photo: photo, token: this.commonService.getCurrentToken() };
    return this.http.post<any>(main_server_url + '/updateAppUserPhoto', body, options);
  }
  // update profile
  updatePassword(user: any) {
    let options = { headers: this.httpHeaders };
    let body: any = user;
    body.token = this.commonService.getCurrentToken();
    return this.http.post<any>(main_server_url + '/updateAppUserPassword', body, options);
  }

  // get app user with id
  getAppUserWithId(token) {
    let body = { token: token }
    let options = { headers: this.httpHeaders };

    return this.http.post<any>(main_server_url + '/getAppUserwithId', body, options);
  }

  // add new review
  addNewReview(review) {
    let options = { headers: this.httpHeaders };
    let body = review;
    body.token = this.commonService.getCurrentToken();
    return this.http.post<any>(main_server_url + '/addNewReview', body, options);
  }
  // add new report
  addNewReport(report) {
    let options = { headers: this.httpHeaders };
    let body = report;
    body.contact_me = body.contact_me ? "true" : "false"
    body.token = this.commonService.getCurrentToken();
    return this.http.post<any>(main_server_url + '/addNewReport', body, options);
  }

  getListingOptionsWithUserid(hostid) {
    let options = { headers: this.httpHeaders };
    let body = { userid: hostid, token: this.commonService.getCurrentToken() };
    return this.http.post<any>(main_server_url + '/getListingOptionsWithUserid', body, options);
  }

  getCoordinates(region) {
    let options = { headers: this.httpHeaders };
    let body = { region: region, token: this.commonService.getCurrentToken() };
    console.log("body:", body);

    return this.http.post<any>(main_server_url + '/getCoordinates', body, options);
  }

  getContactInfoWithId(userid) {
    let body = { userid: userid }
    let options = { headers: this.httpHeaders };
    return this.http.post<any>(main_server_url + '/getContactInfoWithId', body, options);
  }

  getCountryList() {
    return this.http.get(main_server_url + '/getCountrylist');
  }
  async getOptions() {
    return await this.http.get(main_server_url + '/getOptions').toPromise();
  }
  async getRateWithId(host_id) {
    let options = { headers: this.httpHeaders };
    let body = {
      host_id: host_id,
      token: this.commonService.getCurrentToken()
    };
    return await this.http.post<any>(main_server_url + '/getRateWithId', body, options).toPromise();
  }
  async getReviews(host_id) {
    let options = { headers: this.httpHeaders };
    let body = {
      host_id: host_id,
      token: this.commonService.getCurrentToken()
    };
    return await this.http.post<any>(main_server_url + '/getReviews', body, options).toPromise();
  }
  getWhyContent() {
    return this.http.get(main_server_url + '/getWhyContent');
  }
}
