import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { CURRENTAPPUSER, CURRENTAPPUSERTOKEN, TRAVEL_COUNTRY, AppUserClass, HostMarkerClass, SELECTED_HOST_MARKER, server_upload_url } from './common';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  // onTokenBS: BehaviorSubject<string> = new BehaviorSubject<string>('');
  onCurrentAppUserBS: BehaviorSubject<AppUserClass> = new BehaviorSubject<AppUserClass>(null);
  onCurrentTravelCountryBS: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  onSelectedMarkerBS: BehaviorSubject<HostMarkerClass> = new BehaviorSubject<HostMarkerClass>(null);
  constructor(
    private toastController: ToastController
  ) {
    this.getCurrentAppUser();
    this.getCurrentTravelCountry();
    this.getSelectedHostMarkers();
  }

  updateAppUser(currentAppUser: AppUserClass) {
    localStorage.setItem(CURRENTAPPUSER, JSON.stringify(currentAppUser));
    this.onCurrentAppUserBS.next(currentAppUser);
  }
  getCurrentAppUser() {
    let currentUser = JSON.parse(localStorage.getItem(CURRENTAPPUSER));
    this.onCurrentAppUserBS.next(currentUser);
    return currentUser;
  }

  // token
  getCurrentToken() {
    let token = localStorage.getItem(CURRENTAPPUSERTOKEN);
    // this.onTokenBS.next(token);
    return token;
  }
  saveCurrentToken(token: string) {
    localStorage.setItem(CURRENTAPPUSERTOKEN, token);
    // this.onTokenBS.next(token);
  }

  saveCurrentTravelCountry(travelCountry: any) {
    console.log("selected contrylist:", travelCountry);
    localStorage.setItem(TRAVEL_COUNTRY, JSON.stringify(travelCountry));
    this.onCurrentTravelCountryBS.next(travelCountry);
  }
  getCurrentTravelCountry() {
    let currentTravelCountry = JSON.parse(localStorage.getItem(TRAVEL_COUNTRY));
    this.onCurrentTravelCountryBS.next(currentTravelCountry);
    return currentTravelCountry;
  }

  saveSelectedHostMarkers(hostMarker: HostMarkerClass) {
    localStorage.setItem(SELECTED_HOST_MARKER, JSON.stringify(hostMarker));
    this.onSelectedMarkerBS.next(hostMarker);
  }
  getSelectedHostMarkers() {
    let selectedHostMarker = JSON.parse(localStorage.getItem(SELECTED_HOST_MARKER));
    this.onSelectedMarkerBS.next(selectedHostMarker);
  }


  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
