import { Component, OnInit } from '@angular/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { LoadingController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../services/common.service';
import { ApiserviceService } from '../services/apiservice.service';
import { NavigationExtras } from '@angular/router';
import { SELECTED_LANG } from '../services/common';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  uEmail = '';
  validation = '';

  isEmpty = value => value === undefined || value === null || value === '';

  constructor(
    private navCtrl: NavController, 
    private translate: TranslateService, 
    private tConfig: TranslateConfigService,
    private apiService: ApiserviceService, 
    private commonService: CommonService, 
    private loadingController: LoadingController
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
  }


  checkEmail = async() => {
    var check_email = await this.translate.get('common.check_email').toPromise();
    var invalid_email = await this.translate.get('common.invalid_email').toPromise();
    if (!this.isEmpty(this.uEmail) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.uEmail)) {
      this.validation = invalid_email;
    } else {
      this.apiService.forgotPwd(this.uEmail).subscribe((res: any) => {
        if (res['result'] === 1) {
          let navigationExtras: NavigationExtras = {
            state: {
              userid: res['id']
            }
          }
          this.navCtrl.navigateForward('/confirm-recovery-code', navigationExtras);
        } else {
          this.validation = check_email;
        }
      });
    }
  }

}
