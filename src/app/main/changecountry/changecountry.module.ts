import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangecountryPageRoutingModule } from './changecountry-routing.module';

import { ChangecountryPage } from './changecountry.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
    ChangecountryPageRoutingModule
  ],
  declarations: [ChangecountryPage]
})
export class ChangecountryPageModule {}
