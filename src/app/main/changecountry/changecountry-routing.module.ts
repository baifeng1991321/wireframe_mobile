import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChangecountryPage } from './changecountry.page';

const routes: Routes = [
  {
    path: '',
    component: ChangecountryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangecountryPageRoutingModule {}
