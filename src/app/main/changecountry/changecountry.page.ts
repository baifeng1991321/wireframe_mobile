import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { PickerController, NavController, IonSelect } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppUserClass, USERFORM_SIGNUP, TRAVEL_COUNTRY, TravelCountryClass, CURRENTAPPUSER, SELECTED_LANG } from 'src/app/services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-changecountry',
  templateUrl: './changecountry.page.html',
  styleUrls: ['./changecountry.page.scss'],
})
export class ChangecountryPage implements OnInit, OnDestroy {
  currentTravelCountry: TravelCountryClass = new TravelCountryClass([]);
  countrylist: any[] = [];
  appUserClass: AppUserClass = new AppUserClass([]);
  _unsubscribeAll: Subject<any> = new Subject<any>();
  countryForm: FormGroup;
  initCountryFlag: boolean = false;
  @ViewChild('countrySelect', { static: true }) selectRef: IonSelect;
  constructor(
    private pickerCtrl: PickerController, private router: Router, private translate: TranslateService,
    private tConfig: TranslateConfigService, private apiserviceService: ApiserviceService,
    private commonService: CommonService, private navCtrl: NavController
  ) {
    this.appUserClass = new AppUserClass(JSON.parse(localStorage.getItem(CURRENTAPPUSER)));
    this.countryForm = new FormGroup({
      country: new FormControl('')
    });

    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  async ngOnInit() {
    this.apiserviceService.getCountryList().subscribe((res: any[]) => {
      this.countrylist = res;
      this.currentTravelCountry = this.commonService.getCurrentTravelCountry();
      if (this.currentTravelCountry) {
        this.countryForm.controls['country'].setValue(this.currentTravelCountry.text);
      }
    });


  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  openCountry() {
    this.initCountryFlag = true;
    this.selectRef.open();
  }

  changeCountry(e) {
    if (!this.initCountryFlag) return;
    let selectedCountry = this.countryForm.controls['country'].value
    if (selectedCountry) {
      console.log(">>>>>>>>>>>>", selectedCountry);
      let findex = this.countrylist.findIndex(x => { return x.text === selectedCountry });
      this.commonService.saveCurrentTravelCountry(this.countrylist[findex]);
      this.currentTravelCountry = this.countrylist[findex];
    }
  }

  goGmap() {
    this.navCtrl.navigateRoot('/gmap');
    // this.router.navigate(['/gmap']);
  }
}
