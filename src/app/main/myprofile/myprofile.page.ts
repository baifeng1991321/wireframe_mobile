import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { PopoverController, ActionSheetController } from '@ionic/angular';
import { NotifyComponent } from '../notify/notify.component';
import { AppUserClass, SELECTED_LANG } from 'src/app/services/common';
import { CommonService } from 'src/app/services/common.service';
import { takeUntil } from 'rxjs/operators'
import { Subject } from 'rxjs';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit, OnDestroy {


  isLoading = false;
  croppedImageBuffer: any;
  croppedImagepath = "";
  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };

  currentAppUser: AppUserClass = new AppUserClass([]);
  _unsubscribeAll: Subject<any> = new Subject<any>();
  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private popoverCtrl: PopoverController, private commonService: CommonService, public actionSheetController: ActionSheetController,
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
    this.commonService.onCurrentAppUserBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user: AppUserClass) => {
        this.currentAppUser = user;
      })
  }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }


  handleSetLanguage() {
    this.router.navigateByUrl('/select-language');
  }
  handleSettingButton = async (ev) => {
    let popover = await this.popoverCtrl.create({
      component: NotifyComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }
  goEditProfilePage() {
    this.router.navigate(['/editprofile']);
  }

  goToMap() {
    this.router.navigateByUrl('/gmap');
  }
}
