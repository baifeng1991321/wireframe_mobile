import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { AppUserClass, SELECTED_LANG } from 'src/app/services/common';

@Component({
  selector: 'app-why',
  templateUrl: './why.page.html',
  styleUrls: ['./why.page.scss'],
})
export class WhyPage implements OnInit {
  whycontent: any = '';
  currentLang = '';
  constructor(
    private apiService: ApiserviceService, 
    private translate: TranslateService, 
    private tConfig: TranslateConfigService
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.currentLang = current_lang.value.toUpperCase();
  }

  ngOnInit() {
    this.apiService.getWhyContent().subscribe(res => {
      this.whycontent = res[0];
      console.log("why whycontent:", this.whycontent);
      
    });
  }

}
