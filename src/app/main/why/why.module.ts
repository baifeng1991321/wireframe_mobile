import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WhyPageRoutingModule } from './why-routing.module';
import { TranslateModule } from '@ngx-translate/core';

import { WhyPage } from './why.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    WhyPageRoutingModule
  ],
  declarations: [WhyPage]
})
export class WhyPageModule {}
