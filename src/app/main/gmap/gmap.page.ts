
import { Component, OnInit, ViewChild, AfterViewInit, AfterContentInit, OnDestroy } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Router } from '@angular/router';
import { Platform, PopoverController, LoadingController } from '@ionic/angular';
import { SELECTED_LANG } from 'src/app/services/common';
// import { ApiserviceService } from '../../apiservice.service';
import { NotifyComponent } from '../notify/notify.component';
import MarkerClusterer from "@google/markerclusterer";
import { async } from '@angular/core/testing';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';
import { SELECTED_HOST_MARKER, TravelCountryClass, getPhotoUrl, HostMarkerClass } from 'src/app/services/common';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

declare var google: any;
//declare var MarkerClusterer: any;

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.page.html',
  styleUrls: ['./gmap.page.scss'],
})
export class GmapPage implements AfterViewInit, OnDestroy {

  map: any;

  address: string;
  positions = [];
  currentTravelCountry: TravelCountryClass = new TravelCountryClass([]);

  @ViewChild('mapElement', { static: false }) mapElement;
  _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
    private platform: Platform, 
    private geolocation: Geolocation, 
    private popoverCtrl: PopoverController, 
    private nativeGeocoder: NativeGeocoder,
    private router: Router, 
    private apiServ: ApiserviceService, 
    private commonService: CommonService, 
    private loadingController: LoadingController, 
    private translate: TranslateService, 
    private tConfig: TranslateConfigService
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.commonService.onCurrentTravelCountryBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((travelCountry) => {
        this.currentTravelCountry = travelCountry;
        console.log("current travel country:", this.currentTravelCountry);
        // this.loadMap()
        setTimeout(() => {
          this.loadMap()
        }, 1000);
      })
  }

  ngAfterViewInit() {
    // this.loadMap();
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  displayInfoModal = function () {
    this.router.navigateByUrl('/gmapinfo-modal');
  }

  handleSetLanguage() {
    this.router.navigateByUrl('/select-language');
  }

  handleSettingButton = async (ev) => {
    let popover = await this.popoverCtrl.create({
      component: NotifyComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.currentTravelCountry.lat, this.currentTravelCountry.lng);
    let mapOptions = {
      center: latLng,
      zoom: parseInt(this.currentTravelCountry.zoom),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      mapTypeControl: false,
      maxZoom: 15, // for max zoom
      // minZoom: 6, // for min zoom
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.getMarkers();
  }

  async getMarkers() {
    var loading_msg = await this.translate.get('common.loading_msg').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    })
    loading.present();
    
    var btn_more = await this.translate.get('gmap.btn_more').toPromise();
    
    this.apiServ.getCoordinates(this.currentTravelCountry.small_name).subscribe(res => {
      
      if (!res) { return; }
      let res_array = Object.values(res);
      var markers = res_array.slice(0, 100).map((data) => {
        return this.addMarkersMap(data, btn_more);
      });

      var clusterStyles = [{
        textColor: 'transparent',
        url: 'assets/images/gps_marker.png',
        width: 30,
        height: 48,
        backgroundSize: 'cover'
      }];

      var mcOptions = {
        gridSize: 50,
        styles: clusterStyles,
        maxZoom: 15
      }
      var markerCluster = new MarkerClusterer(
        this.map,
        markers,
        mcOptions
      );
      loading.dismiss();
    }, error => {
      console.log("loading get coordinates error:", JSON.stringify(error));
      loading.dismiss();
    });
  }

  addMarkersMap(markers, btn_label) {
    const marker = new google.maps.Marker({
      position: {
        lat: parseFloat(markers['lat']),
        lng: parseFloat(markers['lon'])
      },
      map: this.map
    });
    let name = markers['name'] ? markers['name'] : 'no title';
    let profile_image = markers['profile_image'] ? getPhotoUrl(markers['profile_image']) : '../../../assets/images/nature1.jpg';
    let bio: string = markers['bio'] ? markers['bio'] : 'no bio';
    let viewBio = bio.length > 200 ? bio.substr(0, 200) + "..." : bio;

    

    var contentString = "<div style='width:90vw;'><div class='comment_content' style='display:flex;'>" +
      "<div style='min-width:100px; height:100px; background-size: cover; background-image: url(" + profile_image + ")'></div>" +
      // "<div class='comment_image'><img src='" + profile_image + "' style='width:100px; height:100px;' /></div>" +
      "<div class='comment_sentence' style='padding:5px;'><label style='font-size:15px; font-weight:bold;'>" + name + "</label>" +
      "<p style='font-size:12px;'>" + viewBio + "</p>" +
      "</div></div>" +
      "<button style='margin-top: 5px; height:30px; border-radius:5px; width:100%; text-align:center; background-color:#3880ff;' class='btn-dialog'>" + btn_label + "</button>" +
      "</div>";

    const infoWindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: '100vw'
    });

    marker.addListener('click', function () {
      infoWindow.open(this.map, marker);
    });

    this.map.addListener('tilesloaded', () => {
      this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
    });

    google.maps.event.addListener(infoWindow, 'domready', () => {
      console.log("domready prepare.")
      const xElm: any = document.querySelector('.gm-ui-hover-effect');
      xElm.style.top = '0px'; xElm.style.right = '0px'; xElm.style.width = '34px'; xElm.style.height = '34px';
      const xElmImg: any = document.querySelector('.gm-ui-hover-effect img');
      xElmImg.style.width = '20px'; xElmImg.style.height = '20px';

      const el = document.querySelector('.btn-dialog');
      el.addEventListener('click', (e) => {
        this.commonService.saveSelectedHostMarkers(new HostMarkerClass(markers))
        this.displayInfoModal();
        infoWindow.close();
      });
    });

    return marker;
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords " + lattitude + " " + longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if (value.length > 0)
            responseAddress.push(value);

        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value + ", ";
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) => {
        this.address = "Address Not Available!";
      });

  }

  refreshPage() {
    this.router.navigateByUrl('/gmap');
  }

}
