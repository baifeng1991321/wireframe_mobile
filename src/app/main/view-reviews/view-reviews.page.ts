import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { LoadingController } from '@ionic/angular';
import { getPhotoUrl } from 'src/app/services/common';
import { SELECTED_LANG } from 'src/app/services/common';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';

@Component({
  selector: 'app-view-reviews',
  templateUrl: './view-reviews.page.html',
  styleUrls: ['./view-reviews.page.scss'],
})
export class ViewReviewsPage implements OnInit {
  host_id: any;
  reviews: any[];
  constructor(
    private activateRoute: ActivatedRoute, private apiService: ApiserviceService,
    private loadingController: LoadingController,
    private translate: TranslateService, private tConfig: TranslateConfigService
  ) {
    this.activateRoute.params.subscribe(params => {
      this.host_id = params.host_id;
    })
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
    this.getReviews(this.host_id);
  }

  async getReviews(host_id) {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    })
    loading.present();

    this.reviews = await this.apiService.getReviews(host_id);
    this.reviews.forEach(review => {
      review.photo = getPhotoUrl(review.photo) || ''
    })
    console.log("reviews:", this.reviews);

    loading.dismiss();

  }
}
