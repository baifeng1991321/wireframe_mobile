import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { PopoverController, LoadingController } from '@ionic/angular';
import { NotifyComponent } from '../notify/notify.component';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppUserClass, HostMarkerClass, SELECTED_LANG } from 'src/app/services/common';
import { CommonService } from 'src/app/services/common.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-review-host',
  templateUrl: './review-host.page.html',
  styleUrls: ['./review-host.page.scss'],
})
export class ReviewHostPage implements OnInit, OnDestroy {

  currentAppUser: AppUserClass = new AppUserClass([]);
  selectedHostMarker: HostMarkerClass = new HostMarkerClass([]);
  _unsubscribeAll: Subject<any> = new Subject<any>();
  reviewForm: FormGroup;

  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private popoverCtrl: PopoverController, private apiService: ApiserviceService, private commonService: CommonService,
    private loadingController: LoadingController
  ) {
    this.commonService.onCurrentAppUserBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user) => {
        this.currentAppUser = user;
      })
    this.commonService.onSelectedMarkerBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((hostmarker) => {
        this.selectedHostMarker = hostmarker;
        console.log("selectedHostMarker:", this.selectedHostMarker);
      })
    this.reviewForm = this.getReviewForm();
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }


  handleSetLanguage() {
    this.router.navigateByUrl('/select-language');
  }
  getReviewForm() {
    return new FormGroup({
      user_id: new FormControl(this.currentAppUser.id),
      rate: new FormControl('0', Validators.required),
      host_id: new FormControl(this.selectedHostMarker.id, Validators.required),
      comment: new FormControl('', Validators.required)
    });
  }

  onRateChange(e) {
    console.log(e);
  }

  async addNewReview() {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    })
    loading.present();

    this.apiService.addNewReview(this.reviewForm.getRawValue()).subscribe(res => {
      this.reviewForm.reset();
      this.commonService.presentToast("reviewed successfully.")
      this.router.navigate(['gmap']);
      loading.dismiss();
    }, error => {
      console.error("add new review error:", error);
      loading.dismiss();
    })
  }

  handleSettingButton = async (ev) => {
    let popover = await this.popoverCtrl.create({
      component: NotifyComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

}
