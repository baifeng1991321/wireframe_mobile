import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReviewHostPageRoutingModule } from './review-host-routing.module';

import { ReviewHostPage } from './review-host.page';
import { IonicRatingModule } from 'ionic4-rating';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicRatingModule,
    ReviewHostPageRoutingModule,
    TranslateModule
  ],
  declarations: [ReviewHostPage]
})
export class ReviewHostPageModule { }
