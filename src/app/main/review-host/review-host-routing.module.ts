import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewHostPage } from './review-host.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewHostPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewHostPageRoutingModule {}
