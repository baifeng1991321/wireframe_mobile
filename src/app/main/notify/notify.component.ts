import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController, NavController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { TranslateService } from '@ngx-translate/core';
import { AppUserClass, SELECTED_LANG } from 'src/app/services/common';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss'],
})
export class NotifyComponent implements OnInit {

  constructor(private router: Router, 
    public poCtrl: PopoverController, 
    private navCtrl: NavController,
    private commonServ: CommonService, private translate: TranslateService, private tConfig: TranslateConfigService) {
      let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
      if (current_lang) {
        this.translate.use(current_lang.value);
      } else {
        this.translate.use(this.tConfig.getDefaultLanguage());
      }
    }

  ngOnInit() { }

  goToProfile() {
    this.router.navigateByUrl('/myprofile');
    this.poCtrl.dismiss();
  }

  goToSignin() {
    // this.router.navigateByUrl('/login');
    this.commonServ.updateAppUser(null);
    this.navCtrl.navigateRoot('/login');
    this.poCtrl.dismiss();
  }

  changeCountry() {
    this.router.navigateByUrl('/changecountry');
    this.poCtrl.dismiss();
  }

  haveProblem() {
    this.router.navigateByUrl('/report-host');
    this.poCtrl.dismiss();
  }

  getReason() {
    this.router.navigateByUrl('/why');
    this.poCtrl.dismiss();
  }

}
