import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportHostPageRoutingModule } from './report-host-routing.module';

import { ReportHostPage } from './report-host.page';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ReactiveFormsModule,
    ReportHostPageRoutingModule
  ],
  declarations: [ReportHostPage]
})
export class ReportHostPageModule {}
