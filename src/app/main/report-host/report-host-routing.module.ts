import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportHostPage } from './report-host.page';

const routes: Routes = [
  {
    path: '',
    component: ReportHostPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportHostPageRoutingModule {}
