import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { PopoverController, NavController } from '@ionic/angular';
import { NotifyComponent } from '../notify/notify.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { AppUserClass, TravelCountryClass, SELECTED_LANG } from 'src/app/services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-report-host',
  templateUrl: './report-host.page.html',
  styleUrls: ['./report-host.page.scss'],
})
export class ReportHostPage implements OnInit, OnDestroy {

  currentAppUser: AppUserClass = new AppUserClass([]);
  currentTravelCountry: TravelCountryClass = new TravelCountryClass([]);
  _unsubscribeAll: Subject<any> = new Subject<any>();
  reportForm: FormGroup;
  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private popoverCtrl: PopoverController, private apiService: ApiserviceService, private commonService: CommonService
  ) {
    this.commonService.onCurrentAppUserBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user) => {
        this.currentAppUser = user;
      })

    this.commonService.onCurrentTravelCountryBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((travelCountry) => {
        this.currentTravelCountry = travelCountry;
        console.log("currentTravel country:", this.currentTravelCountry);

      })

    this.reportForm = this.getReportForm();
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }


  handleSetLanguage() {
    this.router.navigateByUrl('/select-language');
  }
  getReportForm() {
    return new FormGroup({
      userid: new FormControl(this.currentAppUser.id),
      region: new FormControl(this.currentTravelCountry.id),
      contact_me: new FormControl('true'),
      message: new FormControl('', Validators.required)
    });
  }
  addNewReport() {
    this.apiService.addNewReport(this.reportForm.getRawValue()).subscribe(res => {
      console.log("report result:", res);

      this.reportForm.reset();
      this.commonService.presentToast("reported successfully.")
      this.router.navigate(['gmap']);
    }, error => {
      console.error("add new review error:", error);
    })
  }
  handleSettingButton = async (ev) => {
    let popover = await this.popoverCtrl.create({
      component: NotifyComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

  goToMap() {
    this.router.navigateByUrl('/gmap');
  }

}
