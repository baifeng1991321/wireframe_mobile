import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { CURRENTAPPUSER, AppUserClass, COUNTRIES, getPhotoUrl, SELECTED_LANG } from 'src/app/services/common';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';
import { CountriesService } from 'src/app/services/countries.service';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ActionSheetController, LoadingController } from '@ionic/angular';
import { CustomValidators } from 'src/app/services/custom-validators';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {
  passwordForm: FormGroup;
  basisProfileForm: FormGroup;
  currentAppUser: AppUserClass = new AppUserClass([]);
  countries: any[] = COUNTRIES;

  isLoading = false;
  croppedImageBuffer: any;
  croppedImagepath = "";
  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };

  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService, private apiService: ApiserviceService,
    private commonService: CommonService, private countryService: CountriesService, private loadingController: LoadingController,
    private file: File, private camera: Camera, private crop: Crop, private actionSheetController: ActionSheetController
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
    this.countries = this.countryService.getAllCountryNames();
  }

  ngOnInit() {
    this.currentAppUser = JSON.parse(localStorage.getItem(CURRENTAPPUSER));
    this.passwordForm = this.getPasswordForm();
    this.basisProfileForm = this.getBasisProfileForm();
  }
  getPasswordForm() {
    return new FormGroup({
      id: new FormControl(this.currentAppUser.id, Validators.required),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
      ]),
      passwordConfirm: new FormControl('', [Validators.required, CustomValidators.confirmPasswordValidator])
    });
  }
  getBasisProfileForm() {
    return new FormGroup({
      id: new FormControl(this.currentAppUser.id),
      email: new FormControl(this.currentAppUser.email),
      password: new FormControl(this.currentAppUser.password),
      active: new FormControl(this.currentAppUser.active),
      first_name: new FormControl(this.currentAppUser.first_name, Validators.required),
      last_name: new FormControl(this.currentAppUser.last_name, Validators.required),
      phone_number: new FormControl(this.currentAppUser.phone_number, Validators.required),
      whatsapp_number: new FormControl(this.currentAppUser.whatsapp_number, Validators.required),
      address: new FormControl(this.currentAppUser.address, Validators.required),
      address2: new FormControl(this.currentAppUser.address2),
      city: new FormControl(this.currentAppUser.city, Validators.required),
      state: new FormControl(this.currentAppUser.state),
      postal_code: new FormControl(this.currentAppUser.postal_code, Validators.required),
      country: new FormControl(this.currentAppUser.country, Validators.required),
      photo: new FormControl(this.currentAppUser.photo),
    });
  }

  async savePhoto() {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    if (this.croppedImagepath) {
      const loading = await this.loadingController.create({
        message: loading_msg
      });
      await loading.present();
      this.apiService.uploadImageData(this.croppedImagepath, this.currentAppUser.id).subscribe((res: any) => {        
        if (res && res.success) {
          this.croppedImagepath = '';
          this.currentAppUser.photo = res.img_file;
          this.basisProfileForm.controls['photo'].setValue(this.currentAppUser.photo);
          this.commonService.updateAppUser(this.currentAppUser);
          loading.dismiss();
        } else {
          loading.dismiss();
        }
      }, error => {
        loading.dismiss();
      })
    }
  }
  async changePassword() {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    if (this.passwordForm.valid) {
      const loading = await this.loadingController.create({
        message: loading_msg
      });
      await loading.present();
      this.apiService.updatePassword(this.passwordForm.getRawValue()).subscribe(res => {
        console.log("update password:", res);


        this.commonService.presentToast("updated successfully.");
        this.passwordForm.reset();
        loading.dismiss();
      }, error => {
        console.error("updated error:", error);
        loading.dismiss();
      });
    }
  }
  updateProfile() {
    if (this.basisProfileForm.valid) {
      this.apiService.updateProfile(this.basisProfileForm.getRawValue()).subscribe(res => {
        console.log("updated profile:", res);
        this.commonService.updateAppUser(this.basisProfileForm.getRawValue());
        this.commonService.presentToast("updated successfully.")
      }, error => {
        console.error("updated error:", error);

      });
    }
  }


  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imagePath: string) => {
      this.cropImage(imagePath);
    }, (err) => {
      console.log("error:", err);
      // alert("get picture error" + err)
    });
  }

  cropImage(fileUrl) {
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          // this.err_msg = JSON.stringify(error);
        })
      .catch(err => {
        alert("crop error:" + err)
      });
  }

  showCroppedImage(imagePath) {
    this.isLoading = true;
    var copyPath = imagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = imagePath.split(imageName)[0];

    this.file.readAsArrayBuffer(filePath, imageName).then(buffer => {
      this.croppedImageBuffer = buffer;
    }, error => {
      // alert("read as array buffer error:" + JSON.stringify(error))
    });

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }
}