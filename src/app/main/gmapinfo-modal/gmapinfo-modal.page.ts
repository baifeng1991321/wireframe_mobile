import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { PopoverController, LoadingController, NavController } from '@ionic/angular';
import { NotifyComponent } from '../notify/notify.component';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HostMarkerClass, Listingoptions, AppUserClass, SELECTED_LANG } from 'src/app/services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-gmapinfo-modal',
  templateUrl: './gmapinfo-modal.page.html',
  styleUrls: ['./gmapinfo-modal.page.scss'],
})
export class GmapinfoModalPage implements OnInit, OnDestroy {

  stars = 0;
  numbers_of_bed: any = 0;
  selectedHostMarker: HostMarkerClass = new HostMarkerClass([]);
  // selectedHostMarker: ContactHostOptionClass = new ContactHostOptionClass([]);
  originalOptions: any[] = [];
  options: any[] = [];
  rules: any[] = [];
  count_reviews = 0;
  currentLang = '';
  list_res: any = {};

  _unsubscribeAll: Subject<any> = new Subject<any>();
  constructor(
    private router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private popoverCtrl: PopoverController, private commonService: CommonService, private apiService: ApiserviceService,
    private callNumber: CallNumber, private sms: SMS, private emailComposer: EmailComposer, private socialSharing: SocialSharing,
    private loadingController: LoadingController, private navController: NavController
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.currentLang = current_lang.value.toUpperCase();

    this.commonService.onSelectedMarkerBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((hostmarker) => {
        this.selectedHostMarker = hostmarker;
        // get checked optinons with userid
        this.getListingOptionsWithUserid(this.selectedHostMarker.userid)
      })
  }

  ngOnInit() {
    // get options from db
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  isOnOptions(option, listingoptions: Listingoptions) {
    let keys = Object.keys(listingoptions);
    return listingoptions[option.field] === 'on';
  }

  async getListingOptionsWithUserid(hostid) {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    })
    loading.present();

    // get review
    console.log('-----------------------------')
    console.log(hostid);
    await this.apiService.getRateWithId(hostid).then(reviews => {
      
      console.log("reviews:", reviews);
      if (reviews) {
        this.stars = parseFloat(reviews.stars) % 1 === 0 ? reviews.stars : reviews.stars.toFixed(1);
        this.count_reviews = reviews.reviews.length;

      }
    });
    console.log("reviews:", this.stars);
    console.log(this.count_reviews);


    // get options list
    let options: any = await this.apiService.getOptions();
    this.originalOptions = options;
    console.log(">>>>>>>>>>>", this.originalOptions);

    this.options = [];

    // get selected options
    this.apiService.getListingOptionsWithUserid(hostid).subscribe(res => {
      console.log(res);
      if (res) {
        let listingoptions: Listingoptions = new Listingoptions(res[0]);
        this.options = []; this.rules = [];
        this.originalOptions.forEach(option => {
          let isOnOption = this.isOnOptions(option, listingoptions);
          option.isChecked = isOnOption;
          if (option.rule_option === 'option' && option.isChecked) {
            this.options.push(option);
          } else if (option.rule_option === 'rule' && option.isChecked) {
            this.rules.push(option);
          }
        });
        loading.dismiss();
        this.list_res = listingoptions;
        console.log("listingoptions => ", listingoptions);
        console.log(this.list_res);
        console.log(options);
      } else {
        loading.dismiss();
      }
    }, error => {
      loading.dismiss();
    })
  }

  viewReviews() {
    if (this.count_reviews >= 5) {
      this.navController.navigateForward(['view-reviews', { host_id: this.selectedHostMarker.id }]);
    }
  }

  onContactHost() {
    if (this.selectedHostMarker.primary_contact_option === 'phone') {
      this.callNumber.callNumber(this.selectedHostMarker.primary_contact, true);
    } else if (this.selectedHostMarker.primary_contact_option === 'text') {
      this.sms.send(this.selectedHostMarker.primary_contact, 'This is my predefined message to you!').then(ress => {
        // alert("ress:" + JSON.stringify(ress))
      }).catch(errrr => {
        alert(JSON.stringify(errrr))
      });
    } else if (this.selectedHostMarker.primary_contact_option === 'email') {
      this.emailComposer.isAvailable().then((available: boolean) => {
        if (available) {
          let email = {
            to: this.selectedHostMarker.primary_contact,
            Subject: 'contact info',
            body: 'contact body',
            isHtml: true
          }
          this.emailComposer.open(email);
        } else {
          alert("sorry, not support email composer!!!");
        }
      })
    } else if (this.selectedHostMarker.primary_contact_option === 'whatsapp') {
      this.socialSharing.canShareVia('whatsapp').then(res => {
        this.socialSharing.shareViaWhatsAppToReceiver(this.selectedHostMarker.primary_contact, 'whatsapp message');
      }).catch((error) => {
        alert("sorry, not support whatsapp: " + JSON.stringify(error));
      })
    }
  }

  handleSetLanguage() {
    this.router.navigateByUrl('/select-language');
  }
  handleSettingButton = async (ev) => {
    let popover = await this.popoverCtrl.create({
      component: NotifyComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

  goToReviewHost() {
    this.router.navigateByUrl('/review-host');
  }

  goToReportHost() {
    this.router.navigateByUrl('/report-host');
  }

  goToMap() {
    this.router.navigateByUrl('/gmap');
  }

}
