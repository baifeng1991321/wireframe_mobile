import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GmapinfoModalPageRoutingModule } from './gmapinfo-modal-routing.module';

import { GmapinfoModalPage } from './gmapinfo-modal.page';
import { TranslateModule } from '@ngx-translate/core';
import {RatingModule} from "ngx-rating";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RatingModule,
    GmapinfoModalPageRoutingModule
  ],
  declarations: [GmapinfoModalPage]
})
export class GmapinfoModalPageModule {}
