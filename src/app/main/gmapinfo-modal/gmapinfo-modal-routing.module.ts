import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GmapinfoModalPage } from './gmapinfo-modal.page';

const routes: Routes = [
  {
    path: '',
    component: GmapinfoModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GmapinfoModalPageRoutingModule {}
