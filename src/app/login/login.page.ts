import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { ApiserviceService } from '../services/apiservice.service';
import { AppUserClass, CURRENTAPPUSER, TravelCountryClass, SELECTED_LANG } from '../services/common';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { site_url } from '../services/common'
import { CommonService } from '../services/common.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  // uName = 'baifeng@outlook.com';
  // uPwd = '123456';
  uName = '';
  uPwd = '';
  validation = '';
  _unsubscribeAll: Subject<any> = new Subject<any>();
  currentTravelCountry: TravelCountryClass = new TravelCountryClass([]);

  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService, private apiServ: ApiserviceService,
    private iab: InAppBrowser, private commonService: CommonService, private navCtrl: NavController, private loadingController: LoadingController
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
    this.commonService.onCurrentTravelCountryBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((travelCountry) => {
        this.currentTravelCountry = travelCountry;
      })
  }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  goToSignUp() {
    // this.router.navigateByUrl('/signup');
    this.navCtrl.navigateForward('/signup');
  }

  async userlogin() {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    });
    await loading.present();
    var reenter_msg = await this.translate.get('login.lbl_re_enter').toPromise();
    var failure_msg = await this.translate.get('login.lbl_login_failure').toPromise();
    this.apiServ.login(this.uName, this.uPwd).subscribe((res: any) => {
      if (res['result'] === 1) {
        let token = res['token'];
        this.commonService.saveCurrentToken(token);
        this.apiServ.getAppUserWithId(token).subscribe(appUser => {
          if (appUser.length) {
            let loggedUser = appUser[0];
            this.commonService.updateAppUser(new AppUserClass(loggedUser));
            // localStorage.setItem(CURRENTAPPUSER, JSON.stringify(new AppUserClass(loggedUser)));
          }
          if (this.currentTravelCountry) {
            this.navCtrl.navigateRoot('/gmap');
          } else {
            this.navCtrl.navigateRoot('/changecountry');
          }
          loading.dismiss();
        });
      } else if (res['result'] === 220) {
        loading.dismiss();
        this.validation = reenter_msg;
      } else {
        loading.dismiss();
        this.validation = failure_msg;
      }
    }, error => {
      loading.dismiss();
    });

  }

  forgotPassword() {
    this.navCtrl.navigateForward(['forgot-password']);
    // this.iab.create(site_url, '_system');
  }

}
