import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SELECTED_LANG } from '../services/common';
import { CommonService } from '../services/common.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../services/translate-config.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public router: Router, 
    private storage: Storage, 
    private commonServ: CommonService,
    private translate: TranslateService, 
    private tConfig: TranslateConfigService) {
      let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
      if (current_lang) {
        this.translate.use(current_lang.value);
      } else {
        this.translate.use(this.tConfig.getDefaultLanguage());
      }
  }

  ngOnInit() {
    setTimeout(() => {
      let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
      console.log("current_lang:", current_lang);

      if (!current_lang) {
        this.router.navigateByUrl('/select-language', { replaceUrl: true });
      } else {
        if(this.commonServ.getCurrentAppUser() !== null) {
          this.router.navigateByUrl('/gmap', {replaceUrl: true});
        } else {
          this.router.navigateByUrl('/login', { replaceUrl: true });
        }
        
      }

      // this.storage.get('set-lang').then((e) => {
      //   if (e !== '' && e != undefined) {
      //     this.router.navigateByUrl('/login', { replaceUrl: true });
      //   } else {
      //     this.router.navigateByUrl('/select-language', { replaceUrl: true });
      //   }
      // });
    }, 3000);
  }

}
