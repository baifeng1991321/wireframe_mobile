import { Component, OnInit } from '@angular/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { LoadingController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../services/common.service';
import { ApiserviceService } from '../services/apiservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUserClass, SELECTED_LANG } from '../services/common';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { CustomValidators } from 'src/app/services/custom-validators';

@Component({
  selector: 'app-create-new-password',
  templateUrl: './create-new-password.page.html',
  styleUrls: ['./create-new-password.page.scss'],
})
export class CreateNewPasswordPage implements OnInit {

  uPwd = '';
  validation = '';
  email: any;
  code: any;

  createPwdForm: FormGroup;
  appUserClass: AppUserClass = new AppUserClass([]);

  constructor(
    private navCtrl: NavController, private translate: TranslateService, private tConfig: TranslateConfigService,
    private apiService: ApiserviceService, private commonService: CommonService, private loadingController: LoadingController,
    private route: ActivatedRoute, private router: Router
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.route.queryParamMap.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.email = this.router.getCurrentNavigation().extras.state.email;
        this.code = this.router.getCurrentNavigation().extras.state.code;
        console.log(this.email);
      }
    });

    this.createPwdForm = this.getCreatePwdForm();
  }

  ngOnInit() {
  }

  getCreatePwdForm() {
    return new FormGroup({
      password: new FormControl(this.appUserClass.password, [
        Validators.required,
        Validators.minLength(8),
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
      ]),
      passwordConfirm: new FormControl('', [Validators.required, CustomValidators.confirmPasswordValidator])
    });
  }

  async goToNext() {
    if (this.createPwdForm.valid) {
      console.log('------------------------');
      console.log(this.createPwdForm.value['password']);

      this.apiService.checkFKey(this.email, this.createPwdForm.value['password'], this.code).subscribe((res) => {
        if (res['result'] === 1) {
          this.router.navigateByUrl('/login', { replaceUrl: true });
        } else {
          this.validation = "Create New Password Failed";
        }
      });
    }
  }

}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

  if (!control.parent || !control) { return null; }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if (!password || !passwordConfirm) { return null; }

  if (passwordConfirm.value === '') { return null; }

  if (password.value === passwordConfirm.value) { return null; }

  return { passwordsNotMatching: true };
};