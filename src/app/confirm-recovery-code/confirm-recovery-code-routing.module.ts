import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmRecoveryCodePage } from './confirm-recovery-code.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmRecoveryCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmRecoveryCodePageRoutingModule {}
