import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfirmRecoveryCodePage } from './confirm-recovery-code.page';

describe('ConfirmRecoveryCodePage', () => {
  let component: ConfirmRecoveryCodePage;
  let fixture: ComponentFixture<ConfirmRecoveryCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmRecoveryCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmRecoveryCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
