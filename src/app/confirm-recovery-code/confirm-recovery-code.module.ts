import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmRecoveryCodePageRoutingModule } from './confirm-recovery-code-routing.module';

import { ConfirmRecoveryCodePage } from './confirm-recovery-code.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ConfirmRecoveryCodePageRoutingModule
  ],
  declarations: [ConfirmRecoveryCodePage]
})
export class ConfirmRecoveryCodePageModule {}
