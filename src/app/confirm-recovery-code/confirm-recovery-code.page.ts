import { Component, OnInit } from '@angular/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { LoadingController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../services/common.service';
import { ApiserviceService } from '../services/apiservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { SELECTED_LANG } from '../services/common';

@Component({
  selector: 'app-confirm-recovery-code',
  templateUrl: './confirm-recovery-code.page.html',
  styleUrls: ['./confirm-recovery-code.page.scss'],
})
export class ConfirmRecoveryCodePage implements OnInit {

  userid = '';
  validation = '';
  uRecoveryCode = '';

  constructor(
    private navCtrl: NavController, private translate: TranslateService, private tConfig: TranslateConfigService,
    private apiService: ApiserviceService, private commonService: CommonService, private loadingController: LoadingController,
    private route: ActivatedRoute, private router: Router
  ) {
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.route.queryParamMap.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.userid = this.router.getCurrentNavigation().extras.state.userid;
        console.log(this.userid);
      }
    })
  }

  ngOnInit() {
  }

  gotoNewPwd = async () => {

    var check_recoverycode = await this.translate.get('common.recovery_code').toPromise();

    this.apiService.checkRCode(this.userid, this.uRecoveryCode).subscribe((res: any) => {
      if (res['result'] === 1) {
        let navigationExtras: NavigationExtras = {
          state: {
            email: res['email'],
            code: this.uRecoveryCode
          }
        }
        this.navCtrl.navigateForward('/create-new-password', navigationExtras);
      } else {
        this.validation = check_recoverycode;
      }
    });

  }

}
