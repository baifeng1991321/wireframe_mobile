import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { PickerController, NavController, IonSelect } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SELECTED_LANG, LANGUAGES, AppUserClass } from '../services/common';
import { CommonService } from '../services/common.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.page.html',
  styleUrls: ['./select-language.page.scss'],
})
export class SelectLanguagePage implements OnInit, OnDestroy {

  sel_lang: any;
  current_lang: any;

  currentAppUser: AppUserClass = new AppUserClass([]);
  _unsubscribeAll: Subject<any> = new Subject<any>();

  languageForm: FormGroup;
  @ViewChild('langSelect', { static: false }) selectRef: IonSelect;

  constructor(
    private pickerCtrl: PickerController, private translateConfigService: TranslateConfigService, private translate: TranslateService,
    public router: Router, private commonService: CommonService, private tConfig: TranslateConfigService, private navCtrl: NavController
  ) {
    this.languageForm = new FormGroup({
      lang: new FormControl('')
    });
    this.current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (this.current_lang) {
      this.translate.use(this.current_lang.value);
      this.languageForm.controls['lang'].setValue(this.current_lang.value)
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }

    this.commonService.onCurrentAppUserBS
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user) => {
        this.currentAppUser = user;
      });

    console.log(this.current_lang);

    this.sel_lang = LANGUAGES;
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  async showBPicker() {
    let opts: PickerOptions = {
      buttons: [
        {
          text: 'Cancel', role: 'cancel'
        },
        {
          text: 'Done', handler: (res) => {
            console.log(res);
            let result = res['language'];
            localStorage.setItem(SELECTED_LANG, JSON.stringify(result));
            this.current_lang = result;
            this.translateConfigService.setLanguage(result.value);
          }
        }
      ],
      columns: [
        {
          name: 'language',
          options: LANGUAGES
        }
      ]
    };

    let picker = await this.pickerCtrl.create(opts);
    picker.present();

  }
  openLanguage() {
    this.selectRef.open();
  }

  changeLanguage(e) {
    console.log(e);
    console.log(JSON.stringify(e));
    let res;
    for (var i = 0; i < this.sel_lang.length; i++) {
      if (this.sel_lang[i].value === e) {
        res = this.sel_lang[i];
      }
    }
    console.log(res);
    localStorage.setItem(SELECTED_LANG, JSON.stringify(res));
    this.current_lang = res;
    this.translateConfigService.setLanguage(res.value);
  }

  goToLogin() {
    if (this.currentAppUser) {
      this.navCtrl.back();
    } else {
      this.router.navigateByUrl('/login');
    }
  }

}
