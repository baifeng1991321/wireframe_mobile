import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterImagePipe } from './filter-image.pipe';



@NgModule({
  declarations: [FilterImagePipe],
  imports: [
    CommonModule
  ],
  exports: [FilterImagePipe],
})
export class PipesModule { }
