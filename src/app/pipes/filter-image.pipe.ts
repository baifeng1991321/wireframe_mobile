import { Pipe, PipeTransform } from '@angular/core';
import { getPhotoUrl } from '../services/common';

@Pipe({
  name: 'filterImage'
})
export class FilterImagePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log("value:", value);
    
    // return getPhotoUrl(value);
    return 'data:image/jpg;base64,' + value;
  }

}
