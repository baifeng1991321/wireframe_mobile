import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'select-language',
    loadChildren: () => import('./select-language/select-language.module').then( m => m.SelectLanguagePageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signups/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'signup2',
    loadChildren: () => import('./signups/signup2/signup2.module').then( m => m.Signup2PageModule)
  },
  {
    path: 'signup3',
    loadChildren: () => import('./signups/signup3/signup3.module').then( m => m.Signup3PageModule)
  },
  {
    path: 'signup4',
    loadChildren: () => import('./signups/signup4/signup4.module').then( m => m.Signup4PageModule)
  },
  {
    path: 'gmap',
    loadChildren: () => import('./main/gmap/gmap.module').then( m => m.GmapPageModule)
  },
  {
    path: 'gmapinfo-modal',
    loadChildren: () => import('./main/gmapinfo-modal/gmapinfo-modal.module').then( m => m.GmapinfoModalPageModule)
  },
  {
    path: 'myprofile',
    loadChildren: () => import('./main/myprofile/myprofile.module').then( m => m.MyprofilePageModule)
  },
  {
    path: 'report-host',
    loadChildren: () => import('./main/report-host/report-host.module').then( m => m.ReportHostPageModule)
  },
  {
    path: 'review-host',
    loadChildren: () => import('./main/review-host/review-host.module').then( m => m.ReviewHostPageModule)
  },
  {
    path: 'editprofile',
    loadChildren: () => import('./main/editprofile/editprofile.module').then( m => m.EditprofilePageModule)
  },
  {
    path: 'changecountry',
    loadChildren: () => import('./main/changecountry/changecountry.module').then( m => m.ChangecountryPageModule)
  },
  {
    path: 'why',
    loadChildren: () => import('./main/why/why.module').then( m => m.WhyPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },  {
    path: 'create-new-password',
    loadChildren: () => import('./create-new-password/create-new-password.module').then( m => m.CreateNewPasswordPageModule)
  },
  {
    path: 'confirm-recovery-code',
    loadChildren: () => import('./confirm-recovery-code/confirm-recovery-code.module').then( m => m.ConfirmRecoveryCodePageModule)
  },
  {
    path: 'view-reviews',
    loadChildren: () => import('./main/view-reviews/view-reviews.module').then( m => m.ViewReviewsPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
