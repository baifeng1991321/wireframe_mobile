import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppUserClass, USERFORM_SIGNUP, COUNTRIES, SELECTED_LANG } from 'src/app/services/common';
import { CountriesService } from 'src/app/services/countries.service';

@Component({
  selector: 'app-signup2',
  templateUrl: './signup2.page.html',
  styleUrls: ['./signup2.page.scss'],
})
export class Signup2Page implements OnInit {
  signupForm: FormGroup;
  appUserClass: AppUserClass = new AppUserClass([]);
  countries: any[] = [];
  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private countryService: CountriesService
  ) {
    this.appUserClass = new AppUserClass(JSON.parse(localStorage.getItem(USERFORM_SIGNUP)));
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
    this.signupForm = this.getSignupForm();
    this.countries = this.countryService.getAllCountryNames();
  }

  ngOnInit() {
  }
  getSignupForm() {
    return new FormGroup({
      email: new FormControl(this.appUserClass.email),
      password: new FormControl(this.appUserClass.password),
      first_name: new FormControl(this.appUserClass.first_name, Validators.required),
      last_name: new FormControl(this.appUserClass.last_name, Validators.required),
      phone_number: new FormControl(this.appUserClass.phone_number),
      whatsapp_number: new FormControl(this.appUserClass.whatsapp_number, Validators.required),
      address: new FormControl(this.appUserClass.address, Validators.required),
      address2: new FormControl(this.appUserClass.address2),
      city: new FormControl(this.appUserClass.city, Validators.required),
      state: new FormControl(this.appUserClass.state),
      postal_code: new FormControl(this.appUserClass.postal_code, Validators.required),
      country: new FormControl(this.appUserClass.country, Validators.required)
      // email: new FormControl('asdf'),
      // password: new FormControl('asdf'),
      // first_name: new FormControl('asdf', Validators.required),
      // last_name: new FormControl('asdf', Validators.required),
      // phone_number: new FormControl('asdfasdf'),
      // whatsapp_number: new FormControl('asdfsdf', Validators.required),
      // address: new FormControl('asdf', Validators.required),
      // address2: new FormControl('.address2'),
      // city: new FormControl('.city', Validators.required),
      // state: new FormControl('.state'),
      // postal_code: new FormControl('.postal_code', Validators.required),
      // country: new FormControl('.country', Validators.required)
    });
  }
  goToNext() {
    if (this.signupForm.valid) {
      localStorage.setItem(USERFORM_SIGNUP, JSON.stringify(this.signupForm.getRawValue()));
      this.router.navigateByUrl('/signup3');
    }
  }

  goToPrevious(){
    this.router.navigateByUrl('/signup');
  }

}
