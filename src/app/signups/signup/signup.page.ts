import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { USERFORM_SIGNUP, AppUserClass, SELECTED_LANG } from '../../services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';
import { CustomValidators } from 'src/app/services/custom-validators';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  appUserClass: AppUserClass = new AppUserClass([]);

  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService,
    private apiService: ApiserviceService, private commonService: CommonService, private loadingController: LoadingController
  ) {
    this.signupForm = this.getSignupForm();
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
    console.log(this.tConfig.getDefaultLanguage());
  }
  getSignupForm() {
    return new FormGroup({
      email: new FormControl(this.appUserClass.email, Validators.email),
      password: new FormControl(this.appUserClass.password, [
        Validators.required,
        Validators.minLength(8),
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
      ]),
      passwordConfirm: new FormControl('', [Validators.required, CustomValidators.confirmPasswordValidator])
      // email: new FormControl('bai@outlook.com', Validators.email),
      // password: new FormControl('123456', Validators.required),
      // passwordConfirm: new FormControl('123456', [Validators.required, confirmPasswordValidator])
    });
  }
  async goToNext() {
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    if (this.signupForm.valid) {
      const loading = await this.loadingController.create({
        message: loading_msg
      });
      await loading.present();
      // chech existing email
      this.apiService.isExistingEmail(this.signupForm.controls['email'].value).subscribe(res => {
        if (parseInt(res) > 0) {
          this.commonService.presentToast("Email duplicated.");
        } else {
          localStorage.setItem(USERFORM_SIGNUP, JSON.stringify(this.signupForm.getRawValue()));
          this.router.navigateByUrl('/signup2');
        }
        loading.dismiss();
      }, error => {
        loading.dismiss();
      })
    }
  }

  goToPrevious(){
    this.router.navigateByUrl('/login');
  }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
// export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

//   if (!control.parent || !control) { return null; }

//   const password = control.parent.get('password');
//   const passwordConfirm = control.parent.get('passwordConfirm');

//   if (!password || !passwordConfirm) { return null; }

//   if (passwordConfirm.value === '') { return null; }

//   if (password.value === passwordConfirm.value) { return null; }

//   return { passwordsNotMatching: true };
// };