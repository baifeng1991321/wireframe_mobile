import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { ActionSheetController, Platform, LoadingController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AppUserClass, USERFORM_SIGNUP, SELECTED_LANG } from 'src/app/services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-signup3',
  templateUrl: './signup3.page.html',
  styleUrls: ['./signup3.page.scss'],
})
export class Signup3Page implements OnInit {

  isLoading = false;
  croppedImageBuffer: any;
  croppedImagepath = "";
  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };
  appUserClass: AppUserClass = new AppUserClass([]);
  constructor(
    public router: Router, private translate: TranslateService, private tConfig: TranslateConfigService, public actionSheetController: ActionSheetController,
    private file: File, private camera: Camera, private crop: Crop, private platform: Platform, private filePath: FilePath, private webview: WebView,
    private apiserviceService: ApiserviceService, private loadingController: LoadingController, private commonService: CommonService,
  ) {
    this.appUserClass = new AppUserClass(JSON.parse(localStorage.getItem(USERFORM_SIGNUP)));
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
  }

  async goToNext() {
    let photourl = "";
    var loading_msg = await this.translate.get('common.lbl_loading').toPromise();
    const loading = await this.loadingController.create({
      message: loading_msg
    });
    await loading.present();

    this.apiserviceService.signup(this.appUserClass).subscribe(res => {
      if (this.croppedImagepath) {
        let insertid = res;
        this.apiserviceService.uploadImageData(this.croppedImagepath, insertid).subscribe((uploadRes: any) => {
          this.commonService.presentToast('registered successfuly');
          this.router.navigateByUrl('/login');
          loading.dismiss();
        }, uploadError => {
          loading.dismiss();
        });
      } else {
        this.commonService.presentToast('registered successfuly');
        this.router.navigateByUrl('/login');
        loading.dismiss();
      }
    }, error => {
      alert(JSON.stringify(error));
      console.error("error create user:", error);
      loading.dismiss();
    });







    // if (this.croppedImagepath) {
    //   this.apiserviceService.uploadImageData(this.croppedImagepath).subscribe((res: any) => {
    //     if (res && res.success) {
    //       photourl = res.img_file;
    //       this.appUserClass.photo = photourl;
    //       this.apiserviceService.signup(this.appUserClass).subscribe(res => {
    //         this.commonService.presentToast('registered successfuly');
    //         this.router.navigateByUrl('/login');
    //         loading.dismiss();
    //       }, error => {
    //         alert(JSON.stringify(error));
    //         console.error("error create user:", error);
    //         loading.dismiss();
    //       });
    //     } else {
    //       this.commonService.presentToast('Image upload error!');
    //       loading.dismiss();
    //     }
    //   });
    // } else {
    //   this.apiserviceService.signup(this.appUserClass).subscribe(res => {
    //     this.commonService.presentToast('registered successfuly');
    //     this.router.navigateByUrl('/login');
    //     loading.dismiss();
    //   }, error => {
    //     alert(JSON.stringify(error));
    //     console.error("error create user:", error);
    //     loading.dismiss();
    //   })
    // }
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imagePath: string) => {
      this.cropImage(imagePath);
    }, (err) => {
      console.log("error:", err);
      // alert("get picture error" + err)
    });
  }

  cropImage(fileUrl) {
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          // this.err_msg = JSON.stringify(error);
        })
      .catch(err => {
        alert("crop error:" + err)
      });
  }

  showCroppedImage(imagePath) {
    this.isLoading = true;
    var copyPath = imagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = imagePath.split(imageName)[0];

    this.file.readAsArrayBuffer(filePath, imageName).then(buffer => {
      this.croppedImageBuffer = buffer;
    }, error => {
      // alert("read as array buffer error:" + JSON.stringify(error))
    });

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      // this.appUserClass.photo = this.croppedImagepath;
      localStorage.setItem(USERFORM_SIGNUP, JSON.stringify(this.appUserClass));
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  goToPrevious(){
    this.router.navigateByUrl('/signup2');
  }
}
