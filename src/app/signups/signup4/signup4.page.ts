import { Component, OnInit } from '@angular/core';
import { PickerController } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { TranslateConfigService } from '../../services/translate-config.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppUserClass, USERFORM_SIGNUP, TRAVEL_COUNTRY, SELECTED_LANG } from 'src/app/services/common';
import { ApiserviceService } from 'src/app/services/apiservice.service';

@Component({
  selector: 'app-signup4',
  templateUrl: './signup4.page.html',
  styleUrls: ['./signup4.page.scss'],
})
export class Signup4Page implements OnInit {
  countrylist: any[] = [];
  selectableCountrylist: any[] = [];
  sel_country = '';
  appUserClass: AppUserClass = new AppUserClass([]);
  constructor(
    private pickerCtrl: PickerController, private router: Router, private translate: TranslateService,
    private tConfig: TranslateConfigService, private apiserviceService: ApiserviceService
  ) {
    this.appUserClass = new AppUserClass(JSON.parse(localStorage.getItem(USERFORM_SIGNUP)));
    let current_lang = JSON.parse(localStorage.getItem(SELECTED_LANG));
    if (current_lang) {
      this.translate.use(current_lang.value);
    } else {
      this.translate.use(this.tConfig.getDefaultLanguage());
    }
  }

  ngOnInit() {
    this.apiserviceService.getCountryList().subscribe((res: any[]) => {
      this.countrylist = res;
      this.countrylist.forEach(element => {
        this.selectableCountrylist.push({
          text: element.text,
          value: element.id
        })
      });
      console.log("countrylist:", this.countrylist);
    });

    this.sel_country = "Australia";
  }

  async showBPicker() {
    let opts: PickerOptions = {
      buttons: [
        {
          text: 'Cancel', role: 'cancel'
        },
        {
          text: 'Done', handler: (res) => {
            let result = res['country'];
            let findex = this.countrylist.findIndex(x => { return x.id === result.value });
            console.log("selected country:", this.countrylist[findex]);
            localStorage.setItem(TRAVEL_COUNTRY, JSON.stringify(this.countrylist[findex]))
            this.sel_country = result.text;
          }
        }
      ],
      columns: [
        {
          name: 'country',
          options: this.selectableCountrylist
        }
      ]
    };

    let picker = await this.pickerCtrl.create(opts);
    picker.present();
  }

  goToLogin() {
    this.apiserviceService.signup(this.appUserClass).subscribe(res => {      
      this.router.navigateByUrl('/login');
    }, error => {
      alert(JSON.stringify(error))
      console.error("error create user:", error);
    })
  }

}
