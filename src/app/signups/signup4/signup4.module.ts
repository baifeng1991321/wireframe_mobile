import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Signup4PageRoutingModule } from './signup4-routing.module';

import { Signup4Page } from './signup4.page';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    Signup4PageRoutingModule
  ],
  declarations: [Signup4Page]
})
export class Signup4PageModule {}
